import math
import timeit
import xlsxwriter

workbook = xlsxwriter.Workbook('append-data.xlsx')
worksheet = workbook.add_worksheet()

def append_test_2():
    N = 100000
    T = 100
    t = []
    s = "Akram"
    # 100 trials
    for i in range(T):
        x = []
        # construct a list from 1 to N elements
        for j in range(N):
            # timer starts and ends and an append call is made in between
            start = timeit.default_timer()
            x.append(s) # append call
            end = timeit.default_timer()
            diff = end - start
            # take the average time diff
            if i == 0:
                t.append(diff)
            else:
                t[i] += diff
    for i in range(N):
        t[i] = t[i]/T

    y = []
    for i in range(1,N):
        #remove resize (O(N)) points
        if t[i] < 10*t[i-1]:
            y.append(t[i])
        
    # write to excel spreadsheet
    for i in range(len(y)):
        worksheet.write(i, 0, i+1)
        worksheet.write(i, 1, y[i])
    workbook.close()

append_test_2()

