import math
import random
import timeit
import xlsxwriter

workbook = xlsxwriter.Workbook('lookup-data.xlsx')
worksheet = workbook.add_worksheet()

def lookups():
    x = []
    t = []
    for i in range(1000000):
        x.append(random.random())
    
    for j in range(1000):
        t.append(newtest(x,j))

    # write to excel spreadsheet
    for i in range(1000):
        worksheet.write(i, 0, i+1)
        worksheet.write(i, 1, t[i])
    workbook.close()

def newtest(x, i):
    y = 0
    for j in range(100):
        t1=timeit.default_timer()
        x[i]
        t2=timeit.default_timer()
        y = y + (t2-t1)
    return y/100

lookups()
    