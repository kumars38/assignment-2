import math
import random
import timeit
from xlsxwriter import Workbook 

workbook = Workbook('copy-data.xlsx')
worksheet = workbook.add_worksheet()

def copy_test():
    x = []
    t=[]

    for i in range(100,10000,100):
        x = make_list(i)
        time_sum=0
        for j in range(100):
            c=[]
            t1= timeit.default_timer()
            c = x.copy()
            t2=timeit.default_timer()
            time_sum += (t2-t1)
        t.append(time_sum/100)

   
    for i in range(1,100):
        worksheet.write(i, 0, i*100)
        worksheet.write(i, 1, t[i-1])
    workbook.close()
def make_list(n):
    x=[]
    for i in range(n):
        x.append(random.random())
    return x
copy_test()