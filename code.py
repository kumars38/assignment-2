import math
import random
import timeit
from xlsxwriter import Workbook 

# copy data
copy_workbook = Workbook('copy-data.xlsx')
copy_worksheet = copy_workbook.add_worksheet()

# lookup data
lookup_workbook = Workbook('lookup-data.xlsx')
lookup_worksheet = lookup_workbook.add_worksheet()

# append data
append_workbook = Workbook('append-data.xlsx')
append_worksheet = append_workbook.add_worksheet()

def copy_test():
    x = []
    t=[]

    for i in range(100,10000,100):
        x = make_list(i)
        time_sum=0
        for j in range(100):
            c=[]
            t1= timeit.default_timer()
            c = x.copy()
            t2=timeit.default_timer()
            time_sum += (t2-t1)
        t.append(time_sum/100)

    # write to excel spreadsheet
    for i in range(1,100):
        copy_worksheet.write(i, 0, i*100)
        copy_worksheet.write(i, 1, t[i-1])
    copy_workbook.close()

def make_list(n):
    x=[]
    for i in range(n):
        x.append(random.random())
    return x

def lookup_test():
    x = []
    t = []
    for i in range(1000000):
        x.append(random.random())

    for j in range(1000000):
        t1=timeit.default_timer()
        x[j]
        t2=timeit.default_timer()
        t.append(t2-t1)

    # write to excel spreadsheet
    for i in range(1000000):
        lookup_worksheet.write(i, 0, i+1)
        lookup_worksheet.write(i, 1, t[i])
    lookup_workbook.close()

def append_test():
    x = []
    t = []
    N = 1000000
    for i in range(N):
        n = i+1
        # timer starts and ends and an append call is made in between
        start = timeit.default_timer()
        x.append(n) # append call
        end = timeit.default_timer()
        diff = end - start
        t.append(diff)

    # write to excel spreadsheet
    for i in range(N):
        append_worksheet.write(i, 0, x[i])
        append_worksheet.write(i, 1, t[i])
    append_workbook.close()

def append_test_2():
    N = 100000   # number of data points
    T = 100      # number of trials
    t = []
    # arbitrary string value to append
    s = "Akram"
    # 100 trials
    for i in range(T):
        x = []
        # construct a list from 1 to N elements
        for j in range(N):
            # timer starts and ends and an append call is made in between
            start = timeit.default_timer()
            x.append(s) # append call
            end = timeit.default_timer()
            diff = end - start
            if i == 0:
                t.append(diff)
            else:
                t[i] += diff
    # get the average time diff at each index
    for i in range(N):
        t[i] = t[i]/T

    y = []
    for i in range(1,N):
        #remove resize (O(N)) points
        if t[i] < 10*t[i-1]:
            y.append(t[i])
        
    # write to excel spreadsheet
    for i in range(len(y)):
        append_worksheet.write(i, 0, i+1)
        append_worksheet.write(i, 1, y[i])
    append_workbook.close()

# method calls    
copy_test()
lookup_test()
append_test()
#append_test_2()