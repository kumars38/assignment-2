import math
import timeit
import xlsxwriter

workbook = xlsxwriter.Workbook('append-data.xlsx')
worksheet = workbook.add_worksheet()

def append_test():
    x = []
    t = []
    N = 1000000
    for i in range(N):
        n = i+1
        # timer starts and ends and an append call is made in between
        start = timeit.default_timer()
        x.append(n) # append call
        end = timeit.default_timer()
        diff = end - start
        t.append(diff)

    # write to excel spreadsheet
    for i in range(N):
        worksheet.write(i, 0, x[i])
        worksheet.write(i, 1, t[i])
    workbook.close()

append_test()

